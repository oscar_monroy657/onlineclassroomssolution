import React, { Fragment } from 'react'
import { useStyles } from "./circleComponentsStyles";

const CircleComponents = () => {
  const classes = useStyles();
  return (
    <Fragment>
      <div className={classes.circleLeft}></div>
      <div className={classes.circleRight}></div>
    </Fragment>
  );
}
 
export default CircleComponents;