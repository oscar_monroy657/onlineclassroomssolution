import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
  
  circleLeft: {
    clipPath: 'circle(35.2% at 0 0)',
    backgroundColor: '#f50057',
    width: '200px',
    height: '200px',
    position: 'absolute',
    top: 0,
    left: 0,
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  },
  circleRight: {
    clipPath: 'circle(35.2% at 100% 100%)',
    backgroundColor: '#f50057',
    width: '200px',
    height: '200px',
    position: 'fixed',
    bottom: 0,
    right: 0,
    [theme.breakpoints.down('sm')]: {
      display: 'none'
    }
  }
}))