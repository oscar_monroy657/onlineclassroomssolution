import React from 'react';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

export const SelectComponent = ({arrayData, labelText, selectValue, changeValueState, selectName}) => {

  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel id="select-component-label">{labelText}</InputLabel>
      <Select
        labelId="select-component-label"
        value={selectValue || ''}
        onChange={changeValueState}
        label={labelText}
        name={selectName}
      >
        <MenuItem value=''>Selecciona una opción</MenuItem>
        {arrayData ?
          arrayData.map((data, i) => (
            <MenuItem value={data} key={i}>{data}</MenuItem>
          ))
        : <MenuItem value="">Selecciona un establecimiento</MenuItem>
        }
      </Select>
    </FormControl>
  );
}

export const SelectSchoolComponent = ({arrayData, labelText, selectValue, changeValueState, changeUserSchool}) => {

  const handleOnChange = e => {
    const id = e.target.value
    const school = arrayData.filter(data => data.id === id)
    changeUserSchool(e)
    changeValueState(school[0])
  }

  return (
    <FormControl variant="outlined" fullWidth margin="normal">
      <InputLabel id="select-component-label">{labelText}</InputLabel>
      <Select
        labelId="select-component-label"
        value={selectValue}
        onChange={handleOnChange}
        label={labelText}
        name="userSchool"
      >
        {arrayData ?
          arrayData.map((data, i) => (
            <MenuItem value={data.id} key={i}>{data.schoolName}</MenuItem>
          ))
        : <MenuItem value="">No hay datos para elegir</MenuItem>
        }
      </Select>
    </FormControl>
  );
}
 