const FormValidate = (userData, userPhoto, userPassword) => {
  let isCorrectly = true
  let message = ''

  if(userData.userName === '' || userData.userLastName === '' || userData.userEmail === '' || userPassword === '' || userData.userGrade === '' || userData.userSection === '' || userData.userShift === '' || userData.userSchool === '') {
    isCorrectly = false
    message = 'Todos los campos son obligatorios'

    return {isCorrectly, message}
  }
  if(!userPhoto.name) {
    isCorrectly = false
    message = 'Por favor selecciona una foto de perfil'

    return {isCorrectly, message}
  }

  return {isCorrectly, message}
}

export default FormValidate