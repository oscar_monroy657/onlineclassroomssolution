import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
  container: {
    [theme.breakpoints.only('xs')]: {
      flexDirection: 'column-reverse'
    },
    [theme.breakpoints.up('xs')]: {
      padding: '3rem 0'
    }
  },
  avatar: {
    width: '100px',
    height: '100px',
    margin: '0 auto',
    backgroundColor: [theme.palette.secondary.main]
  },
  paper : {
    padding: '2rem',
    [theme.breakpoints.down('md')]: {
      padding: '1rem'
    },
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(2),
    }
  },
  itemContainer : {
    padding: '3rem',
    [theme.breakpoints.down('sm')]: {
      padding: '0'
    }
  },
  imgContainer : {
    width: '80%',
    height: '80%',
    backgroundSize: 'cover',
    [theme.breakpoints.down('md')]: {
      width: '100%',
      height: '100%'
    }
  },
  form: {
    paddingBottom: '1.5rem'
  },
  linkContainer: {
    paddingBottom: '1rem'
  },
  link: {
    textDecoration: 'none',
    color: [theme.palette.text.secondary],
    textAlign: 'center'
  },
  selectUserCareer : {
    marginTop: '16px'
  }
}))