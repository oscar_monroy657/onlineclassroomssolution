import React, { useState, useEffect } from 'react';
import { Grid, Container, Button, Paper, TextField, Avatar, CircularProgress } from "@material-ui/core";
import { useStyles } from "./createAccountStyles";
import CircleComponents from "../../components/circles/CircleComponents";
import { firebaseConsumer } from '../../server';
import { SelectComponent, SelectSchoolComponent } from '../../components/selectComponent/SelectComponent';
import initialUserData from '../../utils/initialUserState'
import SelectStudentPhoto from '../../components/selectStudentPhoto/SelectStudentPhoto';
import { useSessionStateValue } from '../../session/SessionContext'
import displaySnackBar from '../../session/actions/snackBarActions'
import { createUserAction } from '../../session/actions/sessionActions';
import FormValidate from './formValidate';
import createStudentLocalKeys from '../../utils/createStudentLocalKeys';

const CreateAccount = props => {
  const classes = useStyles();
  const { firebase, history } = props
  const [ {session}, dispatch ] = useSessionStateValue()

  const [userData, setUserData] = useState(initialUserData)
  const [userPhoto, setUserPhoto] = useState({})
  const [userPassword, setUserPassword] = useState('')

  const [schoolsData, setSchoolsData] = useState([])
  const [schoolSelected, setSchoolSelected] = useState({})
  const [isFirstTime, setIsFirstTime] = useState(true)
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    if(isFirstTime) {
      firebase.getPublicDataFromSchools().then(data => {
        const schoolArray = data.docs.map(doc => {
          const data = doc.data()
          const id = doc.id

          return {id, ...data}
        })
        setSchoolsData(schoolArray)
      })
      .catch(error => console.log(error))
      setIsFirstTime(false)
    }
  }, [isFirstTime, firebase])

  const handleOnChange = e => {
    setUserData({
      ...userData,
      [e.target.name] : e.target.value
    })
  }

  const handleOnSubmit = async e => {
    e.preventDefault()

    const formIsCorrectly = FormValidate(userData, userPhoto, userPassword)

    if(formIsCorrectly.isCorrectly === false) {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: formIsCorrectly.message
      })
      return
    }
    
    if(!schoolSelected.schoolCareers && userData.userCareer !== '') {
      userData.userCareer = ''
    }
    setIsLoading(true)

    createStudentLocalKeys(userData, schoolSelected)

    const result = await createUserAction(dispatch, firebase, schoolSelected, userData, userPassword, userPhoto)

    if(result.status){
      history.push('/dashboard')
    }else {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: result.message
      })
      setIsLoading(false)
    }
    
  }
  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.container}
        alignItems="center"
        justify="center"
      >
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <Paper className={classes.paper}>
            <form className={classes.form} onSubmit={handleOnSubmit}>
              <Avatar className={classes.avatar} src={userPhoto.url}/>
              <SelectStudentPhoto setUserPhoto={setUserPhoto}/>
              <TextField
                variant="outlined"
                label="Nombres"
                name="userName"
                value={userData.userName}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
              />
              <TextField
                variant="outlined"
                label="Apellidos"
                name="userLastName"
                value={userData.userLastName}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
              />
              <SelectSchoolComponent
                arrayData={schoolsData}
                labelText="Establecimiento"
                selectValue={userData.userSchool}
                changeUserSchool={handleOnChange}
                changeValueState={setSchoolSelected}
              />
              <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                  <SelectComponent
                    arrayData={schoolSelected.schoolShifts}
                    labelText="Jornada"
                    selectValue={userData.userShift}
                    changeValueState={handleOnChange}
                    selectName="userShift"
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <SelectComponent
                    arrayData={schoolSelected.schoolLevels}
                    labelText="Grado"
                    selectValue={userData.userGrade}
                    changeValueState={handleOnChange}
                    selectName="userGrade"
                  />
                </Grid>
              </Grid>
              {schoolSelected.schoolCareers ?
              <Grid container className={classes.selectUserCareer} >
                <Grid item xs={12} md={12}>
                  <SelectComponent
                    arrayData={schoolSelected.schoolCareers}
                    labelText="Carrera"
                    selectValue={userData.userCareer}
                    changeValueState={handleOnChange}
                    selectName="userCareer"
                  />
                </Grid>
              </Grid>
              : null
              }
              <TextField
                variant="outlined"
                label="Sección"
                name="userSection"
                value={userData.userSection}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
              />
              <TextField
                type="email"
                variant="outlined"
                label="Email"
                name="userEmail"
                value={userData.userEmail}
                fullWidth
                margin="normal"
                onChange={handleOnChange}
                autoComplete="username"
              />
              <TextField
                variant="outlined"
                type="password"
                label="Contraseña"
                name="userPassword"
                value={userPassword}
                onChange={e => setUserPassword(e.target.value)}
                fullWidth
                autoComplete="current-password"
              />
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  fullWidth
                >
                  {isLoading ? <CircularProgress color="inherit" size={30}/> : 'Crear cuenta' }
                </Button>
            </form>
          </Paper>
        </Grid>
      </Grid>
    </Container>
  );
}
 
export default firebaseConsumer(CreateAccount);