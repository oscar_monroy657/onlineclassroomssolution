import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(theme => ({
  container: {
    [theme.breakpoints.only('xs')]: {
      flexDirection: 'column-reverse',
      alignItems: 'normal'
    },
    [theme.breakpoints.up('xs')]: {
      padding: '3rem 0'
    }
  },
  title: {
    marginBottom: '10px',
    [theme.breakpoints.only('xs')]: {
      textAlign: 'center'
    },
    [theme.breakpoints.down('md')]: {
      fontSize: '2rem',
      marginBottom: '1.2rem',
      marginTop: '1.2rem'
    }
  },
  textMargin:  {
    marginBottom: '10px'
  },
  paper : {
    padding: '2rem',
    [theme.breakpoints.down('md')]: {
      padding: '1rem'
    },
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(2),
    }
  },
  itemContainer : {
    padding: '3rem',
    [theme.breakpoints.down('sm')]: {
      padding: '0'
    }
  },
  imgContainer : {
    width: '100%',
    height: '100%',
    backgroundSize: 'cover',
  },
  form: {
    paddingBottom: '1.5rem',
  },
  buttonContainer : {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  }
}))