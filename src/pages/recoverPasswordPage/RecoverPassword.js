import React, { useState } from "react";
import { Grid, Container, Typography, Button, Paper, TextField, CircularProgress } from "@material-ui/core";
import authentication from "../../draws/authentication.svg";
import { useStyles } from "./recoverPasswordStyles";
import { recoverPasswordTitle, recoverPasswordMessage } from '../../utils/appStrings'
import CircleComponents from "../../components/circles/CircleComponents";
import { firebaseConsumer } from "../../server";
import { useSessionStateValue } from "../../session/SessionContext";
import displaySnackBar from '../../session/actions/snackBarActions'

const RecoverPassword = ({firebase, history}) => {
  const classes = useStyles();
  const [{session}, dispatch] = useSessionStateValue()
  const [userEmail, setUserEmail] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  const handleOnSubmit = e => {
    e.preventDefault()
    setIsLoading(true)
    firebase.auth.sendPasswordResetEmail(userEmail)
    .then(sucess => {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: 'Hemos enviado un correo. Sigue las instrucciones para crear una nueva contraseña.'
      })
    })
    .catch(error => {
      displaySnackBar(dispatch, {
        isOpen: true,
        message: error.message
      })
      setIsLoading(false)
    })

  }
  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.container}
        alignItems="center"
        justify="space-around"
      >
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <Paper className={classes.paper}>
            <Typography component="h1" variant="h3" color="primary" className={classes.title}>
              {recoverPasswordTitle}
            </Typography>
            <Typography component="p" color="primary" className={classes.textMargin}>
              {recoverPasswordMessage}
            </Typography>
            <form className={classes.form} onSubmit={handleOnSubmit}>
              <TextField
                type="email"
                variant="outlined"
                label="Email"
                value={userEmail}
                fullWidth
                margin="normal"
                onChange={e => setUserEmail(e.target.value)}
              />
              <div className={classes.buttonContainer}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                >
                  {isLoading ? <CircularProgress color="inherit" size={30}/> : 'Enviar correo'}
                </Button>
              </div>
            </form>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={5} className={classes.itemContainer}>
          <img src={authentication} className={classes.imgContainer} alt="authLogo"/>
        </Grid>
      </Grid>
    </Container>
  );
};

export default firebaseConsumer(RecoverPassword);
