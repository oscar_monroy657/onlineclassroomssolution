import React from "react";
import { Grid, Container, Typography, Button } from "@material-ui/core";
import { Link } from 'react-router-dom'
import { useStyles } from "./homePageStyles";
import professorIcon from "../../draws/professor.svg";
import CircleComponents from "../../components/circles/CircleComponents";

const HomePage = props => {
  const classes = useStyles();

  return (
    <Container>
      <CircleComponents/>
      <Grid
        container
        spacing={2}
        className={classes.header}
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={6}>
          <Typography
            component="h1"
            variant="h3"
            color="primary"
            className={classes.title}
          >
            Classroom Solution
          </Typography>
          <Typography
            component="p"
            color="primary"
            className={classes.paragraph}
          >
            Bienvenido a la nueva plataforma que te ofrece crear todo un
            ambiente para poder emular un salón de clases.
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} md={5}>
              <Link to="/login" className={classes.link}>
                <Button
                  variant="outlined"
                  color="primary"
                  fullWidth
                >
                  Entrar a mi aula
                </Button>
              </Link>
            </Grid>
            <Grid item xs={12} md={5}>
              <Button
                variant="outlined"
                color="secondary"
                fullWidth
              >
                Más información
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={6}>
          <img src={professorIcon} className={classes.imgContainer} alt="professorLogo"/>
        </Grid>
      </Grid>
    </Container>
  );
};

export default HomePage;
