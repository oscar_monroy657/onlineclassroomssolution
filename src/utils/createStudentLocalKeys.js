const createStudentLocalKeys = (userData, schoolSelected) => {
  const userLocalKeys = {
    schoolId: schoolSelected.id,
    headmasterId: schoolSelected.headmasterId,
    userShift: userData.userShift,
    userGrade: userData.userGrade,
    userSection: userData.userSection,
    userCareer: userData.userCareer
  };

  localStorage.setItem('studentKeys', JSON.stringify(userLocalKeys))
};

export default createStudentLocalKeys
