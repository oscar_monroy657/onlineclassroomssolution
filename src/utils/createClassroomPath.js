const createClassroomPath = (studentData) => {
  const {userGrade, userSection, userCareer} = studentData
  const classRoom = `${userGrade}${userCareer}_${userSection}`

  const classRoomPath = classRoom.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()
  
  return classRoomPath.replace(/\s/g, '_')
}

export default createClassroomPath