const initialUserData = {
  userId: '',
  userName: '',
  userLastName: '',
  userSchool: '',
  userGrade: '',
  userSection: '',
  userCareer: '',
  userShift: '',
  userEmail: '',
  userPhoto: '',
}

export default initialUserData