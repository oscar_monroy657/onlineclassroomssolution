export default function cleanStrings(value) {
  const newValue = value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase()
  return newValue.replace(/\s/g, '_')
 }