import React, { useEffect, useState, useContext } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// App theme
import { MuiThemeProvider } from '@material-ui/core/styles'
import theme from './theme/theme'
// Context and Context Representation
import { FirebaseContext } from './server'
//Session and SanckBar Context
import { useSessionStateValue } from './session/SessionContext'
// Components
import { MainSpinner } from "./components/spinner";
import SnackBar from './components/snackBar/SnackBar';
import HomePage from './pages/homePage/HomePage';
import LoginPage from './pages/loginPage/LoginPage';
import RecoverPassword from './pages/recoverPasswordPage/RecoverPassword';
import CreateAccount from './pages/createAccountPage/CreateAccount';
import UserDashboard from './pages/userDashboard/UserDashboard'
import AuthenticatedRoute from './auth/AuthenticatedRoute';

const App = () => {
  let firebase = useContext(FirebaseContext)
  const [isFirebaseReady, setIsFirebaseReady] = useState(false)
  const [{openSnackBar}, dispatch] = useSessionStateValue()

  useEffect(() => {
    firebase.isReady().then(val => {
      setIsFirebaseReady(val)
    })
  }, [firebase])

  return isFirebaseReady !== false ? (
    <MuiThemeProvider theme={theme}>
      <Router>
        <SnackBar openSnackBar={openSnackBar} dispatch={dispatch} />
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route exact path="/login" component={LoginPage}/>
          <Route exact path="/register" component={CreateAccount}/>
          <Route exact path="/recover-password" component={RecoverPassword}/>
          <AuthenticatedRoute
            exact
            path="/dashboard"
            component={UserDashboard}
            authFirebase={firebase.auth.currentUser}
          />
        </Switch>
      </Router>
    </MuiThemeProvider>
  ) : <MainSpinner/>;
}

export default App;
