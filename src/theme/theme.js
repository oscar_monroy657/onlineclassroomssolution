import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#5e35b1'
    },
    secondary: {
      main: '#f50057'
    }
  }
})

export default theme