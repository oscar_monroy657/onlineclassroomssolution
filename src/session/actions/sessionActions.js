export const createUserAction = ( dispatch, firebase, schoolSelected, userData, userPassword, userPhoto) => {
  const { headmasterId, id } = schoolSelected;
  const photo = userPhoto.data[0];
  const photoName = userPhoto.name;
  const photoExtension = photoName.split(".").pop();

  const profilePhotoName = ("profilePhoto." + photoExtension).replace(/\s/g, "_").toLowerCase();

  return new Promise((resolve, reject) => {
    firebase
      .createUser(userData.userEmail, userPassword)
      .then(auth => {
        userData.userId = auth.user.uid
        firebase.addStudentToFirestore(headmasterId, id, auth.user.uid, userData)
          .then(success => {
            firebase.saveStudentProfilePhoto( profilePhotoName, photo, userData, id, auth.user.uid)
              .then(success => {
                firebase.getUrlStudentProfilePhoto( profilePhotoName, userData, id, auth.user.uid)
                  .then(urlFile => {
                    userData.userPhoto = urlFile;
                    
                    firebase.updateStudentToFirestore( headmasterId, id, auth.user.uid, userData)
                      .then(sucess => {
                        dispatch({
                          type: "REGISTER_USER",
                          school: schoolSelected,
                          user: userData
                        });
                        resolve({ status: true });
                      })
                      .catch(error => {
                        console.log(error);
                        resolve({ status: false, message: error.message });
                      });
                  })
                  .catch(error => {
                    console.log(error);
                    resolve({ status: false, message: error.message });
                  });
              })
              .catch(error => {
                console.log(error);
                resolve({ status: false, message: error.message });
              });
          })
          .catch(error => {
            console.log(error);
            resolve({ status: false, message: error.message });
          });
      })
      .catch(error => {
        console.log(error);
        resolve({ status: false, message: error.message });
      });
  });
};

export const signInAction = ( dispatch, firebase, schoolSelected, userData, userPassword) => {
  const {id, headmasterId} = schoolSelected
  const {userEmail} = userData

  return new Promise((resolve, reject) => {
    firebase.loginUser(userEmail, userPassword)
    .then(auth => {
      firebase.getStudentDataFromFirestore(headmasterId, id, auth.user.uid, userData)
      .then(doc => {
        const student = doc.data()
        dispatch({
          type: 'REGISTER_USER',
          user: student,
          school: schoolSelected
        })
        resolve()
      })
      .catch(error => {
        reject(error.message)
      })
    })
    .catch(error => {
      reject(error.message)
    })
  })
}

export const signOutAction = (dispatch, firebase) => {
  return new Promise((resolve, reject) => {
    firebase.auth.signOut().then(success => {
      dispatch({
        type: "UNREGISTER_USER"
      });
      resolve()
    });
  });
};
