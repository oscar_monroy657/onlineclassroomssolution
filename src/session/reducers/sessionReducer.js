import initialUserData from "../../utils/initialUserState";

const initialState = {
  school: {},
  user: initialUserData,
  isAuthenticated: false
};

const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case "REGISTER_USER":
      return {
        ...state,
        school: action.school,
        user: action.user,
        isAuthenticated: true
      };
    case "UNREGISTER_USER":
      return {
        ...state,
        school: {},
        user: initialState,
        isAuthenticated: false
      };

    default:
      return state
  }
};

export default sessionReducer
