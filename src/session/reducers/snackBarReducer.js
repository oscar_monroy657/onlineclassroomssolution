const initialState = {
  isOpen: false,
  message: ''
}

const snackBarReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN_SNACKBAR':
      return {
        ...state,
        isOpen: action.isOpen,
        message: action.message
      }
  
    default:
      return state
  }
}

export default snackBarReducer