import snackBarReducer from './snackBarReducer'
import sessionReducer from './sessionReducer'

export const mainReducer = ({session, openSnackBar}, action) => {
  return {
    session: sessionReducer(session, action),
    openSnackBar: snackBarReducer(openSnackBar, action)
  }
}