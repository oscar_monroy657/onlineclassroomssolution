import app from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'
import { publicSchoolsCollection } from '../utils/FirestoreNamesKey'
import createClassroomPath from '../utils/createClassroomPath'

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATA_BASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID
};

class FirebaseHelper {
  constructor() {
    app.initializeApp(firebaseConfig)
    this.db = app.firestore()
    this.auth = app.auth()
    this.storage = app.storage()

    this.storage.ref().constructor.prototype.saveDocuments = function(documents, userName, houseName) {
      let ref = this
      const path = `${userName}/housesPhotos/${houseName}`
      return Promise.all(documents.map(file => {
        return ref.child(path+'/'+file.alias).put(file).then(snapshot => ref.child(path+'/'+file.alias).getDownloadURL())
      }))
    }
  }

  isReady() {
    return new Promise(resolve => {
      this.auth.onAuthStateChanged(resolve)
    })
  }

  getPublicDataFromSchools = () => this.db.collection(publicSchoolsCollection).orderBy('schoolName').get()

  createUser = (studentEmail, studentPassword) => {
    return this.auth.createUserWithEmailAndPassword(studentEmail, studentPassword)
  }

  loginUser = (studentEmail, studentPassword) => {
    return this.auth.signInWithEmailAndPassword(studentEmail, studentPassword)
  }

  addStudentToFirestore = async (headMasterId, schoolId, userUid, studentData) => {
    const {userShift} = studentData
    const classRoomClean = createClassroomPath(studentData)

     await this.db.collection(headMasterId).doc(schoolId)
    .collection(userShift).doc(classRoomClean).set({classroomId: classRoomClean}, {merge: true})
    
    return this.db.collection(headMasterId).doc(schoolId).collection(userShift).doc(classRoomClean).collection('Students').doc(userUid).set(studentData, {merge: true})
  }

  updateStudentToFirestore = (headMasterId, schoolId, userUid, studentData) => {
    const {userShift} = studentData
    const classRoomClean = createClassroomPath(studentData)
    
    return this.db.collection(headMasterId).doc(schoolId).collection(userShift).doc(classRoomClean).collection('Students').doc(userUid).set(studentData, {merge: true})
  }
  
  getStudentDataFromFirestore = (headMasterId, schoolId, userUid, studentData) => {
    const {userShift} = studentData
    const classRoomClean = createClassroomPath(studentData)

    return this.db.collection(headMasterId).doc(schoolId).collection(userShift).doc(classRoomClean).collection('Students').doc(userUid).get()
  }
  getSchoolFromFirestore = (headMasterId, schoolId) => {
    return this.db.collection(headMasterId).doc(schoolId).get()
  }

  updateDocumentToFirestore = (collectionName, documentId, data) => {
    return this.db.collection(collectionName).doc(documentId).set(data, {merge:true})
  }


  deleteDocumentFromFirestore = (collectionName, documentId) => {
    return this.db.collection(collectionName).doc(documentId).delete()
  }

  saveStudentProfilePhoto = (fileName, file, studentData, schoolId, userUid) => {
    const {userShift} = studentData
    const classRoomClean = createClassroomPath(studentData)

    const path = `${schoolId}/${userShift}/${classRoomClean}/students/${userUid}/profilePhoto/${fileName}`

    return this.storage.ref().child(path).put(file)
  }

  getUrlStudentProfilePhoto = (fileName, studentData, schoolId, userUid) => {
    const {userShift} = studentData
    const classRoomClean = createClassroomPath(studentData)

    const path = `${schoolId}/${userShift}/${classRoomClean}/students/${userUid}/profilePhoto/${fileName}`

    return this.storage.ref().child(path).getDownloadURL()
  }

  saveFilesInStorage = (documents, userName, houseName) => this.storage.ref().saveDocuments(documents, userName, houseName)

  deleteFileInStorage = (fileName, userName,  houseName) => {
    const path = `${userName}/housesPhotos/${houseName}`

    return this.storage.ref().child(path+'/'+fileName).delete()
  }
}

export default FirebaseHelper